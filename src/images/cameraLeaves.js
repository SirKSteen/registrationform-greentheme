import React from 'react';
import camera from "../images/cameraLeaves.jpg";
import './cameraLeaves.css';

export let CameraInLeaves = () => {
    <div >
        <img src={camera} className={"cameraPic"} />
    </div>
}