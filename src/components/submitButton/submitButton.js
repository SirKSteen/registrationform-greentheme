import React from "react";
import './submitButton.css';

export let SubmitButton = (props) => {
    return (
        <div className={"pageCardSubmitButton"} >
            <p className={"buttonText"}>
                {props.name}
            </p>
        </div>
    )
}