import React from "react";
import "./header.css";

export let Header = (props) => {
  return (
    <div className={"pageCardHeader"}>
      <h1>{props.name}</h1>
      <h3>{props.details}</h3>
    </div>
  );
};
