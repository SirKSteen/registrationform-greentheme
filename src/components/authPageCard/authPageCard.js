import React from "react";
import "./authPageCard.css";
import { AuthPageCardLeft } from "../authPageCardLeft/authPageCardLeftContainer";
import { AuthPageCardRight } from "../authPageCardRight/authPageCardRightContainer";

export let AuthPageCard = () => {
  return (
    // <div className={"authPageCardWrapper"} >
    //     <div className={"authPageCardContainer"} >
    //         <authPageCardLeft className={"leftCard"}/>
    //         <authPageCardRight className={"rightCard"} />
    //     </div>
    // </div>

    <div className={"authPageCardWrapper"}>
      <div className={"authPageCardContainer"}>
        <AuthPageCardLeft />
        <AuthPageCardRight />
      </div>
    </div>
  );
};
