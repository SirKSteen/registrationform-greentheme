import React from "react";

import "./authPageCardLeftContainer.css";
// import { CameraInLeaves } from "../images/cameraLeaves";

import leftImage from '../../images/cameraLeaves.jpg';

export let AuthPageCardLeft = () => {
  return( 
    <div className={"pageCardLeftContainer"}>
        <img src={leftImage} alt="Camera in leaves" 
          id={"pageCardLeftContainerImage"}  
        />      
    
    </div>
  );
};
