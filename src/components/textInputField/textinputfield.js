import React from "react";

import "./textinputfield.css";

export let TextInputField = (props) => {
  return (
    <div className={"textInputFieldContainer"}>
      <label>{props.labelName}</label>
      <input placeholder={props.inputName} />
    </div>
  );
};
