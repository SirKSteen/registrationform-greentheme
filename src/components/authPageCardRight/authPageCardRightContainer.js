import React from "react";

import "./authPageCardRightContainer.css";
import { Header } from "../header/header";
import { TextInputField } from "../textInputField/textinputfield";
import {SubmitButton} from "../submitButton/submitButton";

export let AuthPageCardRight = () => {
  return (
    <div className={"pageCardRightContainer"}>
      <Header name="Registration" details="CLICK FOR YOUR SHOOT" />
      <TextInputField labelName="Name" inputName="Tanvir Messan" />
      <TextInputField labelName="Email" inputName="harivan23@gmail.com" />
      <TextInputField labelName="Date of Birth" inputName="May 25, 1977" />
      <TextInputField labelName="City" inputName="Ohake" />
      <TextInputField labelName="Country" inputName="Bangladesh" />
      <div className={"pageCardButtonContainer"} >
          <SubmitButton name="Register"/>
      </div>
    </div>
  );
};
