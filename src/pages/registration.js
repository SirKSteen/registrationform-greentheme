import React from 'react';
import './registration.css';

import {AuthBackground} from "../components/authBackground/authBackgroundContainer";
import {AuthPageCard} from '../components/authPageCard/authPageCard';
// import {AuthPageCard} from "../components/authPageCard";

export let Registration = () => {
    return (
        <div className={"authBackgroundPageContainer"} >
            <AuthBackground/>
            <AuthPageCard/>
        </div>
    )
}