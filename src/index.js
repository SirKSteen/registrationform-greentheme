import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {Registration} from "./pages/registration";


ReactDOM.render(
  <React.StrictMode>
    <Registration/>
  </React.StrictMode>,
  document.getElementById('root')
);
